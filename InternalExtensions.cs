﻿using System;
using System.Linq;
using System.Reflection;

namespace SFML.Utils
{
    static class InternalExtensions
    {
        public static N CreateDelegate<N>(this Type type, string name) where N : class
        {
            var methods = type.GetMethods().Concat(type.GetMethods(BindingFlags.NonPublic | BindingFlags.Instance));
            var method = methods.First(x => x.Name == name);
            return Delegate.CreateDelegate(typeof(N), null, method) as N;
        }
        public static void CreateDelegate<N>(this Type type, out N ptr, string name) where N : class
        {
            ptr = CreateDelegate<N>(type, name);
        }
    }
}