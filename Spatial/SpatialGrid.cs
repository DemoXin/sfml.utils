﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using SFML.Graphics;
using SFML.Window;
using SFML.System;


namespace SFML.Utils.Spatial
{
    public class SpatialGrid<T> : ISpatialMap<T> where T : ISpatial
    {
        class Bucket : HashSet<T>
        {
            public Vector2i Key;

            public override string ToString()
            {
                return "Bucket " + Count + " " + Key;
            }
        }

        public SpatialGrid(float width, float height, float size)
        {
            Width = width;
            Height = height;
            Size = size;
            buckets = new Bucket[(int)(Width / Size) + 1, (int)(Height / Size) + 1];
        }

        private readonly float Width = 800;
        private readonly float Height = 600;

        private Stack<Bucket> pool = new Stack<Bucket>();
        private Bucket[,] buckets;
        private Dictionary<T, FloatRect> items = new Dictionary<T, FloatRect>();

        public readonly float Size = 20;

        public IEnumerable<T> Select(FloatRect region)
        {
            foreach (var bucket in GetBuckets(region, false))
            {
                foreach (var item in bucket)
                {
                    //var aabb = items[item];
                    if (region.Intersects(item.AABB)) yield return item;
                    //if (Intersects(item.AABB, region)) yield return item;
                }
            }
        }

        public IEnumerable<T> Select(Vector2f v)
        {
            var key = ToKey(v.X, v.Y);
            return buckets[key.X, key.Y] ?? Enumerable.Empty<T>();
        }

        public void Add(T item)
        {
            item.AABBChanged += item_AABBChanged;
            var rec = item.AABB;
            items.Add(item, rec);

            foreach (var bucket in GetBuckets(rec, true))
            {
                bucket.Add(item);
            }
        }

        public void Clear()
        {
            items.Clear();
            var toRemove = buckets.OfType<Bucket>().ToArray();
            foreach (var bucket in toRemove)
            {
                RemoveBucket(bucket);
            }
        }

        public bool Contains(T item)
        {
            return items.ContainsKey(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public int Count { get; private set; }
        public bool IsReadOnly { get; private set; }

        void item_AABBChanged(ISpatial spatial)
        {
            var obj = (T)spatial;
            var aabb = obj.AABB;
            var oldBuckets = GetBuckets(items[obj], false).ToArray();
            items[obj] = aabb;
            var newBuckets = GetBuckets(aabb, true).ToArray();

            foreach (var bucket in newBuckets.Except(oldBuckets))
                bucket.Add(obj);

            foreach (var bucket in oldBuckets.Except(newBuckets))
            {
                bucket.Remove(obj);
                if (bucket.Count == 0) RemoveBucket(bucket);
            }

        }
        public bool Remove(T item)
        {
            var rec = items[item];
            items.Remove(item);
            item.AABBChanged -= item_AABBChanged;
            foreach (var bucket in GetBuckets(rec, false))
            {
                bucket.Remove(item);
                if (bucket.Count == 0)
                    RemoveBucket(bucket);
            }

            return true;
        }

        void RemoveBucket(Bucket bucket)
        {
            Debug.Assert(bucket.Count == 0);
            buckets[bucket.Key.X, bucket.Key.Y] = null;
            if (pool.Count < 100)
                pool.Push(bucket);
        }

        Vector2i ToKey(float x, float y)
        {
            x /= Size;
            y /= Size;
            x = x.Clamp(0, buckets.GetLength(0)-1);
            y = y.Clamp(0, buckets.GetLength(1)-1);
            return new Vector2i((int)x, (int)y);
        }

        IEnumerable<Bucket> GetBuckets(FloatRect rec, bool createMissing)
        {
            var b = ToKey(rec.Left, rec.Top);
            var e = ToKey(rec.Right(), rec.Bottom());

            for (int x = b.X; x <= e.X; x++)
                for (int y = b.Y; y <= e.Y; y++)
                {
                    var key = new Vector2i(x, y);
                    Bucket bucket = buckets[x, y];
                    if (bucket != null)
                    {
                        yield return bucket;
                    }
                    else if (createMissing)
                    {
                        if (pool.Count > 0)
                            bucket = pool.Pop();

                        else bucket = new Bucket();
                        buckets[x, y] = bucket;
                        bucket.Key = key;
                        yield return bucket;
                    }
                }
        }

        public IEnumerator<T> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}