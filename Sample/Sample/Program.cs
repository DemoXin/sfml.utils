﻿using SFML.Graphics;
using SFML.System;
using SFML.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample
{
    class MyGame : Game
    {
        SpriteBatch sb;
        Texture blank;
        float rotation;

        protected override void Initialize()
        {
            sb = new SpriteBatch();
            blank = new Texture(1, 1);

            //texture can be updated with Color array
            blank.Update(new[] { Color.White }, new IntRect(0, 0, 1, 1));
        }

        protected override void Draw(float dT)
        {
            Window.Clear();
            sb.Begin();

            var center = Window.GetView().Size / 2;

            //spritebatch increases drawing performance
            //useful for large amount of sprites, like tilemaps
            sb.Draw(blank, center, new IntRect(0, 0, 1, 1), Color.Yellow, center, new Vector2f(0.5f,0.5f), rotation);
            rotation += dT*180;
            if (rotation > 360) rotation = 0;

            sb.End();
            Window.Draw(sb);
            Window.Display();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var settings = new GameSettings();
            var game = new MyGame();
            game.Run(settings);
        }
    }
}
